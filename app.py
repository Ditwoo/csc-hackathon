from os.path import join
from flask import Flask, request, render_template, jsonify, flash, redirect
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '../data'
ALLOWED_EXTENSIONS = {'jpg', 'png', 'bmp'}

app = Flask(__name__, template_folder='./templates')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def is_allowed_file(filename: str) -> bool:
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def entry_point():
    return render_template('base.html')


@app.route('/parse-check', methods=['GET', 'POST'])
def check_parser():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('There is no attached file!')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file!')
            return redirect(request.url)
        if file and is_allowed_file(file.filename):
            filename = join(app.config['UPLOAD_FOLDER'], secure_filename(file.filename))
            file.save(filename)
            print(f' * Saved image to {filename}', flush=True)
            return render_template('image_uploaded_successful.html', image_name=filename)
    return render_template('image_checks_parser.html')


@app.route('/uploaded')
def image_uploaded():
    return render_template('image_uploaded_successful.html')


@app.route('/tag-text')
def tag_text():
    return render_template('text_tagger.html')
